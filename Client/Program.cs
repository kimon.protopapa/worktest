﻿using ServiceContract;
using System.Collections.Generic;
using System;
using System.ServiceModel;
using Service;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            ChannelFactory<IHighscores> channel = new ChannelFactory<IHighscores>("Highscores");

            IHighscores proxy = channel.CreateChannel();
            Console.WriteLine("Client connected: Enter to close");

            proxy.ClearScores();
            Console.WriteLine("should be nothing from here");
            Console.WriteLine("to here");

            proxy.SaveScore(new Score("test", 4));
            proxy.SaveScore(new Score("test2", 5));
            PrintList(proxy.GetScores());

            Console.ReadLine();
        }

        static void PrintList(List<Score> list)
        {
            foreach(Score score in list)
            {
                Console.WriteLine(score.name + ":  " + score.time);
            }

        }
    }

}