﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using Dapper;
using System.Data.SqlClient;

namespace ServiceContract
{  
   
    public class Highscores : IHighscores
    {
        private const string ConnString = "server=localhost;user id=vs;database=database;password=vs";
        public void ClearScores()
        {
            Console.WriteLine("Clearing Scores");
            using (var connection = new MySqlConnection(ConnString))
            {
                connection.Query("TRUNCATE highscores");
            }

        }

       
        public List<Score> GetScores()
        {
            Console.WriteLine("Fetching Scores");
            List<Score> temp;
            using (var connection = new MySqlConnection(ConnString)){
                var scores = connection.Query<Score>("SELECT * FROM highscores");
                temp = scores.ToList();
            }
            return temp;
        }

        public void SaveScore(Score score)
        {
            Console.WriteLine($"Saving Score with name {score.name} and time {score.time}");

            using (var connection = new MySqlConnection(ConnString))
            {
                connection.Query($"INSERT INTO highscores VALUES ('{score.name}','{score.time}')");
            }
        }
    }
}
