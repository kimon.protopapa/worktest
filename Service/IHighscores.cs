﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceContract
{
    
    [ServiceContract]
    public interface IHighscores
    {
        [OperationContract]
        List<Score> GetScores();

        [OperationContract]
        void SaveScore(Score score);

        [OperationContract]
        void ClearScores();
    }
}
