﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    [DataContract]
    public class Score
    {

        public Score(string name, int score)
        {
            this.name = name;
            this.time = score;
        }

        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int time { get; set; }
    }
}
