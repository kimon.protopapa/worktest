﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContract
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(Highscores)))
            {
                host.Open();
                Console.WriteLine("Server: Enter to close");
                Console.ReadLine();
            }
        }
    }
}
