﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ClientApp
{

    class Taquin
    {
        public readonly int SIDE_LENGTH;
        public List<Piece> SOLVED;
        private List<Piece> pieces;

        public Taquin(int side=3)
        {
            
            SIDE_LENGTH = side;
            List<Piece> temp = new List<Piece>(Enum.GetValues(typeof(Piece)).Cast<Piece>());
            temp = temp.GetRange(1, SIDE_LENGTH * SIDE_LENGTH - 1);
            temp.Add(Piece.HOLE);
            SOLVED = new List<Piece>(temp);
            pieces = new List<Piece>(temp);
        }

        public int PosToIndex(int x, int y)
        {
            return y * SIDE_LENGTH + x;
        }


        public ReadOnlyCollection<Piece> Pieces()
        {
            return new ReadOnlyCollection<Piece>(pieces);
        }

        public void Shuffle(int n=1)
        {

            Random random = new Random();
            int ind = random.Next() % (SIDE_LENGTH * SIDE_LENGTH);

            for (int i = 0; i < n; i++)
            {
                while (!CanSwap(ind))
                {
                    ind = random.Next() % (SIDE_LENGTH * SIDE_LENGTH);
                }
                Swap(ind);
            }
        }

        public bool IsSolved()
        {
            return pieces.SequenceEqual(SOLVED);
        }

        public bool CanSwap(int index)
        {
            return IndexOfNeighboringHole(index) != -1;
        }

        public void Swap(int index)
        {
            Piece temp = pieces[index];
            pieces[index] = pieces[IndexOfNeighboringHole(index)];
            pieces[IndexOfNeighboringHole(index)] = temp;
        }

        private bool IsValidPos(int x, int y)
        {
            return (0 <= x && x < SIDE_LENGTH)
                    && (0 <= y && y < SIDE_LENGTH);
        }

        private bool IsHoleAt(int x, int y)
        {
            return IsValidPos(x, y) && pieces[PosToIndex(x, y)] == Piece.HOLE;
        }

        public int IndexOfNeighboringHole(int index)
        {
            int x = index % SIDE_LENGTH;
            int y = index / SIDE_LENGTH;

            if (IsHoleAt(x - 1, y))
                return PosToIndex(x - 1, y);
            if (IsHoleAt(x + 1, y))
                return PosToIndex(x + 1, y);
            if (IsHoleAt(x, y - 1))
                return PosToIndex(x, y - 1);
            if (IsHoleAt(x, y + 1))
                return PosToIndex(x, y + 1);
            return -1;
        }

        public string ToString(int index)
        {
            switch(pieces[index])
            {
                case Piece.HOLE: return " ";
                default:
                    return ((int)pieces[index]).ToString();
            }
        }
    }
}