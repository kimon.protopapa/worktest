﻿using System.ServiceModel;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;

namespace ClientApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {

        GridLayout grid;
        Taquin game;

        Button shuffle;
        Button hard;
        Button scores;

        TextView winText;
        TextView onlineText;

        Chronometer chrono;
        Button[] gridButtons;

        HighscoresClient client;
        bool online = true;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);

            grid = FindViewById<GridLayout>(Resource.Id.Grid);
            game = new Taquin();

            shuffle = FindViewById<Button>(Resource.Id.shuffle);
            hard = FindViewById<Button>(Resource.Id.hard);
            scores = FindViewById<Button>(Resource.Id.highscores);

            chrono = FindViewById<Chronometer>(Resource.Id.chrono);
            winText = FindViewById<TextView>(Resource.Id.winText);
            onlineText = FindViewById<TextView>(Resource.Id.online);
            onlineText.Text = "online";

            gridButtons = new Button[game.SIDE_LENGTH * game.SIDE_LENGTH];
            InitializeClient();
            InitializeGrid();
            InitializeOthers();

        }
        
        private void InitializeClient()
        {
            EndpointAddress addr = new EndpointAddress("http://85.6.166.244:8733/Highscores");
            
            try
            {
                client = new HighscoresClient(new BasicHttpBinding(), addr);
                client.Open();
                client.GetScores();
            } catch (EndpointNotFoundException e)
            {
                online = false;
                onlineText.Text = "offline";
            }
        }
        private void InitializeGrid()
        {
            for (int i = 0; i < game.SIDE_LENGTH * game.SIDE_LENGTH; i++)
            {
                gridButtons[i] = (Button)grid.GetChildAt(i);
                int index = i;

                gridButtons[i].Click += (sender, e) =>
                {
                    if (game.CanSwap(index))
                    {
                        string temp = gridButtons[index].Text;
                        gridButtons[index].Text = "";
                        gridButtons[game.IndexOfNeighboringHole(index)].Text = temp;
                        game.Swap(index);
                    }

                    if (game.IsSolved())
                    {
                        chrono.Stop();
                        winText.Text = "YOU WIN";
                        Service.Score score = new Service.Score
                        {
                            time = (int)(SystemClock.ElapsedRealtime() - chrono.Base) / 1000,
                            name = "test"
                        };
                        client.SaveScore(score);
                    }
                };
            }
        }
        private void InitializeOthers()
        {
            shuffle.Click += (sender, e) =>
            {
                game.Shuffle(100);

                for (int i = 0; i < game.SIDE_LENGTH * game.SIDE_LENGTH; i++)
                {
                    string temp = ((int)game.Pieces()[i]).ToString();
                    ((Button)grid.GetChildAt(i)).Text = temp.Equals("0") ? "" : temp;
                }

                chrono.Base = SystemClock.ElapsedRealtime();
                chrono.Start();
            };

            hard.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(HardActivity));
                StartActivity(intent);
            };

            if(online)
            {
                scores.Click += (sender, e) =>
                {
                    var intent = new Intent(this, typeof(EasyScores));
                    StartActivity(intent);
                };
            }
        }
	}
}

