﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ClientApp
{
    [Activity(Label = "HardActivity")]
    public class HardActivity : Activity
    {
        GridLayout grid;
        Taquin game;

        Button shuffle;
        Button easy;
        Button scores;

        TextView winText;
        Chronometer chrono;
        Button[] gridButtons;

        HighscoresClient client;
        bool online = true;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_hard);

            grid = FindViewById<GridLayout>(Resource.Id.GridHard);
            game = new Taquin(4);

            shuffle = FindViewById<Button>(Resource.Id.shuffleHard);
            easy = FindViewById<Button>(Resource.Id.easy);
            scores = FindViewById<Button>(Resource.Id.highscoresHard);

            winText = FindViewById<TextView>(Resource.Id.WinTextHard);
            chrono = FindViewById<Chronometer>(Resource.Id.chrono);

            gridButtons = new Button[game.SIDE_LENGTH * game.SIDE_LENGTH];

            InitializeClient();
            InitializeGrid();
            InitializeOthers();
        }
        private void InitializeClient()
        {
            EndpointAddress addr = new EndpointAddress("http://172.20.127.22:8733/Highscores");
            try
            {
                client = new HighscoresClient(new BasicHttpBinding(), addr);
            }
            catch (EndpointNotFoundException e)
            {
                online = false;
            }
        }

        private void InitializeGrid()
        {
            for (int i = 0; i < game.SIDE_LENGTH * game.SIDE_LENGTH; i++)
            {
                gridButtons[i] = (Button)grid.GetChildAt(i);
                int index = i;

                gridButtons[i].Click += (sender, e) =>
                {
                    if (game.CanSwap(index))
                    {
                        string temp = gridButtons[index].Text;
                        gridButtons[index].Text = "";
                        gridButtons[game.IndexOfNeighboringHole(index)].Text = temp;
                        game.Swap(index);
                    }

                    if (game.IsSolved())
                    {
                        chrono.Stop();
                        winText.Text = "YOU WIN";
                    }
                };
            }
        }

        private void InitializeOthers()
        {
            shuffle.Click += (sender, e) =>
            {
                game.Shuffle(200);

                for (int i = 0; i < game.SIDE_LENGTH * game.SIDE_LENGTH; i++)
                {
                    string temp = ((int)game.Pieces()[i]).ToString();
                    ((Button)grid.GetChildAt(i)).Text = temp.Equals("0") ? "" : temp;
                }

                winText.Text = "";
                chrono.Base = SystemClock.ElapsedRealtime();
                chrono.Start();
            };

            easy.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
            };

            if (online)
            {
                scores.Click += (sender, e) =>

                {
                    //var intent = new Intent(this, typeof(EasyScores));
                    //StartActivity(intent);
                };
            }
        }
    }
}