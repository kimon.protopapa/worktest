package mono.android.app;

public class XamarinAndroidEnvironmentVariables
{
	// Variables are specified the in "name", "value" pairs
	public static final String[] Variables = new String[] {
		"MONO_LOG_LEVEL", "info",
		"XAMARIN_BUILD_ID", "e9fa60b2-ce99-4d21-b53b-8e8aee986da0",
		"XA_HTTP_CLIENT_HANDLER_TYPE", "Xamarin.Android.Net.AndroidClientHandler",
		"XA_TLS_PROVIDER", "btls",
		"MONO_GC_PARAMS", "major=marksweep-conc",

	};
}
